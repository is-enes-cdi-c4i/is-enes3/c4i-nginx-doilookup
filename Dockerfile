FROM nginx
WORKDIR /usr/share/nginx/html

COPY ./nginx.conf /etc/nginx/nginx.conf.template

EXPOSE 80

# Set the command to start the node server.
CMD envsubst '${DOILOOKUPENDPOINT}' < /etc/nginx/nginx.conf.template > /etc/nginx/nginx.conf && nginx -g 'daemon off;'



